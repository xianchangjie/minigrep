use std::error::Error;
use std::{env, fs};

pub struct Config {
    query: String,
    file_path: String,
    ignore_case: bool,
}

impl Config {
    pub fn build(mut args: impl Iterator<Item = String>) -> Result<Config, String> {
        // if args.len() < 3 {
        //     return Err(String::from("Not enough Args"));
        // }
        args.next();

        let query = match args.next() {
            Some(arg) => arg,
            None => return Err(String::from("dont get query string"))
        };
        let file_path = match args.next() {
            Some(arg) => arg,
            None => return Err(String::from("dont get file_path string"))
        };
        // let file_path = args[2].clone();
        let ignore_case = env::var("IGNORE_CASE").is_ok();
        Ok(Config { query, file_path, ignore_case })
    }
}

pub fn search(query: String, ignore_case: bool, content: String) -> Vec<String> {
    content.lines().filter(|line| line.contains(&query)).map(|re|String::from(re)).collect()
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let text: String = fs::read_to_string(&config.file_path)?;
    let res = search(config.query, config.ignore_case, text);
    println!("search result:{:?}", res);
    Ok(())
}


#[cfg(test)]
mod tests {
    use crate::search;

    #[test]
    fn test_run() {
        assert_eq!(vec!["To an admiring bog!"], search(String::from("To"),
                                                       false,
                                                       String::from("To an admiring bog!
传遍整个无聊的沼泽！")));
    }
}
