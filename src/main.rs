use minigrep::{run, Config};
use std::{env, process};

fn main() {
    // let args: Vec<String> = .collect();
    let config = Config::build(env::args()).unwrap_or_else(|err| {
        eprintln!("Error happened: {err}");
        process::exit(1);
    });

    if let Err(e) = run(config) {
        eprintln!("App error: {e}");
        process::exit(1);
    }
}
